import axios from 'axios';

export default axios.create({
    baseURL: process.env.VUE_APP_API_URL || "http://10.168.26.9:8080",
});
